#include "DeviceDO.h"



DeviceDO::DeviceDO(const int _Type_id, const string _Name, const SensorState _State, const bool _isDigital, const string _Location, const time_t _DateLastActive, const time_t _DateInstalled):
	Type_id(_Type_id),
	Name(_Name),
	State(_State),
	isDigital(_isDigital),
	Location(_Location),
	DateLastActive(_DateLastActive),
	DateInstalled(_DateInstalled)
{}


DeviceDO::~DeviceDO()
{
}
