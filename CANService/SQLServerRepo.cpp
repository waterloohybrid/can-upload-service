#include "SQLServerRepo.h"



SQLServerRepo::SQLServerRepo()
{
}


SQLServerRepo::~SQLServerRepo()
{
}

void SQLServerRepo::ConnectToDatabase()
{
	SAConnection con;
	con.setClient(SA_SQLServer_Client);
	try {
		con.Connect(DB, USER, PWD);
	}
	catch (SAException  &)
	{
		cout << "Connection Failed - Exception thrown!" << endl;
		try
		{
			// on error rollback changes
			con.Rollback();
		}
		catch (SAException &)
		{
			cout << "Rollback Failed" << endl;
		}
		return;
	}

	cout << "Connected" << endl;
	try {
		SACommand cmd(&con,
			"INSERT INTO [dbo].[Sensor.Type] ([SensorType_id],[Type],[Name],[Description]) VALUES (1,1,'Temperature', 'Measures Temperature')"
		);
		cmd.Execute();
	}
	catch (SAException &)
	{
		cout << "SQL Command Failed" << endl;
		return;
	}
	con.Disconnect();
}
