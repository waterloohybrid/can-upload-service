#pragma once

enum VehicleState {
	Disabled,
	Standby,
	SelfTest,
	PreCharged,
	Energizd,
	EMEnabled,
	Discharging,
};