#include "Tester.h"

using namespace Postgresql;

Tester::Tester()
{
}


Tester::~Tester()
{
}

void Tester::exit_safe(PGconn * conn)
{
	PQfinish(conn);
}

bool Tester::PostgresqlConnectionTest()
{
	Postgresql *server = new Postgresql();
	PGconn *conn;

	try {
		conn = PQconnectdb(server->getConnInfo());
	}
	catch (std::exception &)
	{
		std::cout << "Could Not execute connection!" << std::endl;
		exit_safe(conn);
		return false;
	}
	if (PQstatus(conn) != CONNECTION_OK) {
		ConnStatusType check = PQstatus(conn);
		std::cout << "Failed Connetion" << std::endl;
		std::cout << PQerrorMessage(conn) << std::endl;
		fprintf(stderr, "Connection to database failed: %s",
			PQerrorMessage(conn));
		exit_safe(conn);
		return false;
	}
	std::cout << "Connected!" << std::endl;
	exit_safe(conn);
	return true;
}
