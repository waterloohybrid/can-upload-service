#pragma once

#include "SensorState.h"
#include<iostream>
#include<ctime>

using namespace std;

class DeviceDO
{
private:
	int Device_id;

public:
	DeviceDO(const int _Type_id, const string _Name, const SensorState _State, const bool _isDigital, const string _Location, const time_t _DateLastActive, const time_t _DateInstalled);
	~DeviceDO();

	int Type_id;
	string Name;
	SensorState State;
	bool isDigital;
	string Location;
	time_t DateLastActive;
	time_t DateInstalled;
};

