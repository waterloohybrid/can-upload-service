#pragma once

#include <iostream>
#include <ctime>

#include "VehicleStateEnum.h"
#include "DataDO.h"
#include "RunCycleDO.h"

class IRepoistory
{
public:
	virtual ~IRepoistory() {}

	virtual void ConnectDB() = 0;

	virtual void DisconnectDB() = 0;

	virtual bool CheckConnectionStatus() = 0;

	virtual void CreateSensorData(int Cycle_id, int Device_id, float DataField, time_t TimeStamp, VehicleState State) = 0;

	virtual void CreateSensorData(DataDO entry) = 0;

	virtual void CraeteRunCycle(time_t TimeStart, time_t TimeEnd, std::string Errors, int RunType) = 0;

	virtual void CreateRunCycle(RunCycleDO entry) = 0;
};