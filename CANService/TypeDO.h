#pragma once

#include<iostream>

using namespace std;

class TypeDO
{
private:
	int Type_id;

public:
	TypeDO(const string _Type, const string _Name, const string _Description);
	~TypeDO();

	string Type;
	string Name;
	string Description;
};

