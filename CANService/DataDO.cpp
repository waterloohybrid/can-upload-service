#include "DataDO.h"



DataDO::DataDO(const int _Cycle_id, const int _Device_id, const VehicleState _VehicleState, const float _DataField, const time_t _TimeStamp) :
	Cycle_id(_Cycle_id), 
	Device_id(_Device_id),
	TheVehicleState(_VehicleState),
	DataField(_DataField),
	TimeStamp(_TimeStamp)
{}

DataDO::~DataDO()
{
}

int DataDO::GetDataID()
{
	return this->data_id;
}

bool DataDO::IsDataIDNull()
{
	return data_id == NULL;
}
