#pragma once

#include <iostream>

#include "libpq-fe.h"
#include "Postgresql.h"

namespace Postgresql{
	class Tester
	{
	public:
		Tester();
		~Tester();

		static void exit_safe(PGconn *conn);
		static bool PostgresqlConnectionTest();
	};
}


