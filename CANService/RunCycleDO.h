#pragma once

#include <iostream>
#include <ctime>

using namespace std;

class RunCycleDO
{
private:
	int Cycle_id;

public:
	RunCycleDO(const string _RunType, const time_t _StartTime, const time_t _EndTime, const string _Errors, const int _StartBarttery, const int _EndBattery);
	~RunCycleDO();

	string RunType;
	time_t StartTime;
	time_t EndTime;
	string Errors;
	int StartBattery;
	int EndBattery;
};

