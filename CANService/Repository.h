#pragma once

#include <iostream>

#include "IRepoistory.h"
#include "Postgresql.h"

namespace Postgresql {

	class Repository : public IRepoistory
	{
	public:
		Repository();
		~Repository();
	private:

		Postgresql *server;
		// Inherited via IRepoistory
		virtual void ConnectDB() override;

		virtual void DisconnectDB() override;

		virtual bool CheckConnectionStatus() override;

		virtual void CreateSensorData(int Cycle_id, int Device_id, float DataField, time_t TimeStamp, VehicleState State) override;

		virtual void CreateSensorData(DataDO entry) override;

		virtual void CraeteRunCycle(time_t TimeStart, time_t TimeEnd, std::string Errors, int RunType) override;

		virtual void CreateRunCycle(RunCycleDO entry) override;

	};

}