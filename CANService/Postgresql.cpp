#include "Postgresql.h"

Postgresql::Postgresql::Postgresql()
{
}

Postgresql::Postgresql::~Postgresql()
{
	//Deleting Pointer Approach - Incorrect
	delete this->conninfo;
	delete this->conn;
	delete this->results;
}

const char* Postgresql::Postgresql::getConnInfo() noexcept
{
	return this->conninfo;
}

PGconn* Postgresql::Postgresql::getConnection() noexcept
{
	return this->conn;
}
