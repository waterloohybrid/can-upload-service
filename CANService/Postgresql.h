#pragma once

#include <iostream>
#include <stdlib.h>

#include "libpq-fe.h"

namespace Postgresql {
	class Postgresql {
	public:
		Postgresql();
		~Postgresql();
		const char* getConnInfo() noexcept;
		PGconn* getConnection() noexcept;

	private:
		const char *conninfo = "user=CANService password=Test123 dbname=WFE hostaddr=127.0.0.1 port=5432";
		PGconn *conn;
		PGresult *results;
	};
}
