#pragma once
#include <iostream>

#include "Postgresql.h"

static class ErrorHandler
{
private:
	void PrintException() noexcept;

public:
	ErrorHandler();
	~ErrorHandler();

};

