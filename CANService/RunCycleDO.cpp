#include "RunCycleDO.h"



RunCycleDO::RunCycleDO(const string _RunType, const time_t _StartTime, const time_t _EndTime, const string _Errors, const int _StartBarttery, const int _EndBattery):
	RunType(_RunType),
	StartTime(_StartTime),
	EndTime(_EndTime),
	Errors(_Errors),
	StartBattery(_StartBarttery),
	EndBattery(_EndBattery)
{}


RunCycleDO::~RunCycleDO()
{
}
