#pragma once

#include <iostream>
#include <ctime>
#include "VehicleStateEnum.h"

class DataDO
{
private:
	int data_id = NULL;

public:
	DataDO(const int _Cycle_id, const int _Device_id, const VehicleState _VehicleState, const float _DataField, const time_t _TimeStamp);
	~DataDO();

	int GetDataID();
	bool IsDataIDNull();

	int Cycle_id;
	int Device_id;
	VehicleState TheVehicleState;
	float DataField;
	time_t TimeStamp;
};

