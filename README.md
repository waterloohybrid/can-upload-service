# README #

## What is this repository for? ##

#### Summary ####
* The Service will capture CAN Frames from a wired/wireless transceiver. The frames will be parsed and uploaded to database for GUI to extract.
 
#### Version ####
* Version 1.0

## Set Up ##
### Project Access ###
* Clone Repo
* Create your own branch with naming convention [Initial].[TypeOfWork] ie "rm.databaseRepository"

### Software Requirements ###
#### Required ####
* Visual Studio Community 2015 - IDE - https://www.visualstudio.com/vs/community/
* Postgresql 6.9.X (Latest Version) - Database Engine - https://www.postgresql.org/download/
* (Should come with the download of postgres) pgAdmin 4.1 (or later) - Development Platform for PostgreSQL - https://www.postgresql.org/ftp/pgadmin3/pgadmin4/v1.1/
* NI-CAN Drivers - CAN API - http://search.ni.com/nisearch/app/main/p/bot/no/ap/tech/lang/en/pg/1/sn/ssnav:drv/q/ni-can/
* SQLAPI - API used for Database Access for various Database Engines - http://www.sqlapi.com/
#### Optional ####
* SQL Server Express 2016 (Optional) - Database Engine - http://www.microsoft.com/en-cy/sql-server/sql-server-editions-express
* SMSS 2016 - Development Platform for SQLServer - https://msdn.microsoft.com/en-us/library/mt238290.aspx

## Configuration ##
### Library - Dependencies ###
Include the following Libraries in the project:

* nicanmsc.lib - Found from NI-CAN Driver Installation .\National Instruments\NI-CAN\MS Visual C
* libpq.lib - Found within Postgresql ..\PostgreSQL\9.6\lib
* sqlapi.lib, sqlapid.lib, sqlapis.lib, sqlapisd.lib - Found within SQLAPI ..\SQLAPI\vs2015\lib

Include the following .dlls in Debug file:

* libpq.dll - ..\PostgreSQL\9.6\lib
* sqlapi.dll - ..\SQLAPI\vs2015\bin
* sqlapid.dll - ..\SQLAPI\vs2015\bin

### Database configuration ###
* Open pgAdmin
* Create server with name = WFE, Host name/address = localhost and password to postgres (If cannot create server make sure that postgres 9.6.x is installed)
* Create new login with name = CANService, password = 123test
* Create new database with the name WFE
* Click the new database (WFE) Tools -> Query Tool paste the provided SQL script
* Run the script

### How to run tests ###
* TBA

## Contribution guidelines ##

#### Collaboration Notes ####
* Work in your own Branch - Merge to master once approved
* Ensure Building solution before committing 
* Clearly summarize changes in commit comment

## Who do I talk to? ##
### Admins ###
* Raj or Bradley